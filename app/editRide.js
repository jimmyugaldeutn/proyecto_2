/* set user name*/
let user = JSON.parse(localStorage.getItem('user'));
document.getElementById("user").innerHTML = user.fName;
/* select ride*/
let rideS = JSON.parse(localStorage.getItem('rideSelect'));
//set info of ride
document.getElementById("rideNme").value = rideS.rideNme;
document.getElementById("textArea").value = rideS.textArea;
document.getElementById("departure").value = rideS.departure;
document.getElementById("arrival").value = rideS.arrival;

//set check buttons
for (let i = 0; i <= 8; i++) {
    if (rideS.days[i] == "true") {
        document.getElementById(i).checked = true;
    }
}
//set cbx from
let trends = document.getElementById('from'), trend, i;
for (i = 0; i < trends.length; i++) {
    trend = trends[i];
    if (trend.text == rideS.start) {
        document.getElementById("from").options.item(i).selected = 'selected';
    }
}
//set cbx to
let trends2 = document.getElementById('to'), trend2, j;
for (j = 0; j < trends2.length; j++) {
    trend2 = trends2[j];
    if (trend2.text == rideS.end) {
        document.getElementById("to").options.item(j).selected = 'selected';
    }
}

//save select ride
function saveRide(){

    let lstart = document.getElementById('from');
    let lend = document.getElementById('to');
    let rideNme = document.getElementById('rideNme').value;
    let start = lstart.options[lstart.selectedIndex].text;
    let end = lend.options[lend.selectedIndex].text;
    let textArea = document.getElementById('textArea').value;
    let departure = document.getElementById('departure').value;
    let arrival = document.getElementById('arrival').value;

    let days = [];
    for (let i = 0; i <= 6; i++) {
        var checkbox = document.getElementById(String(i));
        days.push(checkbox.checked.toString());
    }

    let sRide = JSON.parse(localStorage.getItem('rideSelect'));

    let rides = JSON.parse(localStorage.getItem('rides'));

    for (var i = 0; i < rides.length; i++) {
        console.log(rides[i].id);
        console.log(sRide.id);

        if (rides[i].id == sRide.id) {

            rides[i].rideNme =  rideNme;
            rides[i].start =  start;
            rides[i].end =  end;
            rides[i].textArea =  textArea;
            rides[i].departure =  departure;
            rides[i].arrival =  arrival;
            rides[i].days =  days;

            sRide.rideNme =  rideNme;
            sRide.start =  start;
            sRide.end =  end;
            sRide.textArea =  textArea;
            sRide.departure =  departure;
            sRide.arrival =  arrival;
            sRide.days =  days;
        }
    }

    localStorage.setItem('rides', JSON.stringify(rides));
    localStorage.setItem('rideSelect', JSON.stringify(sRide));
}

function bindEvents() {

    jQuery('#login-button').bind('click', function (element) {

        saveRide();
        window.location.href = "/pages/dashboard.html";

    });


}

bindEvents();