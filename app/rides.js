//set user
let user = JSON.parse(localStorage.getItem('user'));
document.getElementById("user").innerHTML = user.fName;
const RIDERSKEY = 'rides';

//create new ride
function insertRide() {
    let user = document.getElementById('user').innerText;
    console.log(user);

    let rideNme = document.getElementById('rideNme').value;

    let lstart = document.getElementById('from');
    let lend = document.getElementById('to');

    let start = lstart.options[lstart.selectedIndex].text;
    let end = lend.options[lend.selectedIndex].text;

    let textArea = document.getElementById('textArea').value;

    let departure = document.getElementById('departure').value;
    let arrival = document.getElementById('arrival').value;

    let days = [];
    for (let i = 1; i <= 7; i++) {
        var checkbox = document.getElementById(String(i));
        days.push(checkbox.checked.toString());
    }

    let currentKey = localStorage.getItem('ridesLastInsertedId');

    if (!currentKey) {
        localStorage.setItem('ridesLastInsertedId', 1);
        currentKey = 1;
    } else {
        currentKey = parseInt(currentKey) + 1;
        localStorage.setItem('ridesLastInsertedId', currentKey);
    }

    const ride = {
        user,
        rideNme,
        start,
        end,
        textArea,
        departure,
        arrival,
        days,
        id: currentKey
    };

    console.log(ride);

    let rides = JSON.parse(localStorage.getItem(RIDERSKEY));

    if (rideNme == '' || start == '' || end == '' || textArea == '' || departure == '' || arrival == '') {
        alert("error");
    } else {
        if (rides && rides.length > 0) {
            rides.push(ride);
        } else {
            rides = [];
            rides.push(ride);
        }
        localStorage.setItem(RIDERSKEY, JSON.stringify(rides));
    }

    clearFields();
}

//clear blanks
function clearFields() {
    document.getElementById('rideNme').value = '';
    document.getElementById('textArea').value = '';
    document.getElementById('departure').value = '';
    document.getElementById('arrival').value = '';
}

function bindEvents() {
	jQuery('#login-button').bind('click', function (element) {
		insertRide();
        window.location.href = "/pages/dashboard.html";
        console.log("hi2");

	});

}

bindEvents();

