//set user
let user = JSON.parse(localStorage.getItem('user'));
document.getElementById("user").innerHTML = user.fName;
//fill blanks
document.getElementById("name").value = user.fName;
document.getElementById("speed").value = user.speedAverage;
document.getElementById("txta").innerHTML = user.aboutMe;
//save user Settings
function saveSett() {
    let suser = JSON.parse(localStorage.getItem('user'));
    let users = JSON.parse(localStorage.getItem('users'));

    let name = document.getElementById('name').value;
    let speed = document.getElementById('speed').value;
    let about = document.getElementById('txta').value;


    for (var i = 0; i < users.length; i++) {

        if (users[i].id == suser.id) {
            console.log("hi2");
            users[i].user = name;
            users[i].speedAverage = speed;
            users[i].aboutMe = about;

            user.user = name;
            user.speedAverage = speed;
            user.aboutMe = about;
            
        }
    }

    localStorage.setItem('users', JSON.stringify(users));
    localStorage.setItem('user', JSON.stringify(user));


}

function bindEvents() {
    jQuery('#login-button').bind('click', function (element) {
      saveSett();
      window.location.href = "/pages/dashboard.html";
  
    });
  
  }
  
  bindEvents();

