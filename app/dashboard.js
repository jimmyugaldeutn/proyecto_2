/* set user name*/
let user = JSON.parse(localStorage.getItem('user'));
document.getElementById("user").innerHTML = user.fName;
/* fill table*/
showListOfRides();

//read all rides of the user/
function showListOfRides() {
    let user = JSON.parse(localStorage.getItem('user'));

    let rides = JSON.parse(localStorage.getItem('rides'));

    let table = document.getElementById('ridesTable');

    if (!rides) {
        rides = [];
    } else {
        for (var i = 0; i < rides.length; i++) {

            if (rides[i].user != user.fName) {
                rides.splice(i, 1);
                i--;
            }
        }
    }

    if (rides) {
        let rows = "";
        rides.forEach((ride, index) => {
            let row = `<tr>`;
            row += `<td>${ride.user}</td>`;
            row += `<td>${ride.start}</td>`;
            row += `<td>${ride.end}</td>`;
            row += `<td> <a onclick="showRide(this)" data-id="${ride.id}"" href="#">Edit</a>  |  <a  onclick="deleteRide(this);" data-id="${ride.id}" class="#" href="#">Delete</a>  </td>`;
            rows += row + "</tr>";
        });
        table.innerHTML = rows;
    }

}

/* set the select ride*/
function showRide(element) {
    //creo una tabla temporal para editar/
    let sRide = jQuery(element).data();

    let rides = JSON.parse(localStorage.getItem('rides'));

    if (!rides) {
        rides = [];
    } else {
        for (var i = 0; i < rides.length; i++) {
            console.log(rides[i].id);
            console.log(sRide.id);

            if (rides[i].id == sRide.id) {
                localStorage.setItem("rideSelect", JSON.stringify(rides[i]));
            }
        }
    }

    window.location.href = "/pages/editRide.html";
}

/* remove the select ride of the list*/
function deleteRide(element) {

    let sRide = jQuery(element).data();

    let rides = JSON.parse(localStorage.getItem('rides'));

    for (var i = 0; i < rides.length; i++) {
        console.log(rides[i].id);
        console.log(sRide.id);

        if (rides[i].id == sRide.id) {
            rides.splice(i, 1);
            i--;
        }
    }

    localStorage.setItem('rides', JSON.stringify(rides));
    showListOfRides();
}

/* btn actions*/
function bindEvents() {
    jQuery('#btn1').bind('click', function (element) {
        window.location.href = "rides.html";
    });

    jQuery('#btn2').bind('click', function (element) {
        window.location.href = "rides.html";

    });


}

bindEvents();