const USERSKEY = 'users';

//save user
function insertUser() {

	let fName = document.getElementById('lfirsName').value;
	let lName = document.getElementById('llastName').value;
    let phone = document.getElementById('lPhone').value;
    let userName = document.getElementById('luserName').value;
    let pass = document.getElementById('lPass').value;
    let repeatPass = document.getElementById('lrepeatPass').value;
    
	let currentKey = localStorage.getItem('usersLastInsertedId');

	if (!currentKey) {
		localStorage.setItem('usersLastInsertedId', 1);
		currentKey = 1;
	} else {
		currentKey = parseInt(currentKey) + 1;
		localStorage.setItem('usersLastInsertedId', currentKey);
	}

	const user = {
		fName,
		lName,
		phone,
        userName,
        pass,
        speedAverage:"60km/h",
        aboutMe:"Something about me goes here",
		id: currentKey
	};

	let users = JSON.parse(localStorage.getItem(USERSKEY));

    if(fName == '' || lName == '' || phone == '' || userName == '' || pass == '' || repeatPass == ''){
        alert("error");
    } else{
        if (users && users.length > 0) {
            users.push(user);
        } else {
            users = [];
            users.push(user);
        }
        localStorage.setItem(USERSKEY, JSON.stringify(users));
    }

	clearFields();
}

//clear blanks
function clearFields() {
	document.getElementById('lfirsName').value = '';
    document.getElementById('llastName').value = '';
    document.getElementById('lPhone').value = '';
    document.getElementById('luserName').value = '';
    document.getElementById('lPass').value = '';
    document.getElementById('lrepeatPass').value = '';
	
}


function bindEvents() {
	jQuery('#login-button').bind('click', function (element) {
		insertUser();
        window.location.href = "/pages/login.html";
        console.log("hi2");

	});

}

bindEvents();
